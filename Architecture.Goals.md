# Architecture Goals 

## Implementable

Able to prototype the architecture and demonstrate relatively easily.

### Value 
`This is both to validate the architecture but also allow others to take up the architecture.`

### How
Deployed to cloud

## Automated

It should be possible to provision the infrastructure of the architecture in a completely automated and timely fashion.

This is required in order to ensure that any instance of the architecture has the expected characteristics. 
The intention is to be able to validate the architecture has a certain security risk profile and given a review and consideration of this we want any architectural deployment to likewise acheive the same characteristics as the prototype.

[Further](./InfrastructureProvisioning.Capability.md)

### Value: 
`The value with this is to acheive reproducilibity, agilenss and cost`


## Incremental

It must be possible to be able to build up the infrastructure as a series of incremental additions to a simplified starting infrastructure.

### Value
`The value here is the ability to progress the system functionality while minimizing testing activity required to maintain quality`

### How to


## Changes are made largely through additions

Made possible by dynamic routing and discovery mechanism
As functions are deployed they should register with the service registry where they find out available services they can utilize.

### Value
`The value here is to be able to deploy with confidence and changes are introduced gradually`

### How

## Maintain Deployment Options

[This video describes one role of a solution architect is to sell options](https://www.youtube.com/watch?v=mS0AJLqmnvQ). The option to be able to support changes into the future.

For this architecture we want to retain the ability to deploy code in different tiers.
Code that might have been considered back end we might want to deploy in the front end and vice versa.

A consequence of this is going to be that we want to use a language that runs equally well in the front or back end.

## Serverless

Avoids the use of dedicated compute that costs money when not in use and also represents a security risk.

[See this content](./FunctionDeployment.Capability.md) related to investigating how to deploy functions into the cloud.

## Change queue

All changes are enqueued (CQRS) style such that they can be consumed by multiple consumers

## Robustness

An architecture that has the possibility to easily add redundancy including
- using CDN to distribute static contect
- redundant service end points 
- distributed queuing
- replication of persistent storage

## Deployable from CI

This cloud deployment should be automatable from the CD pipeline. 

## Runnable Locally

The application should be runnable on a developers machine.
This is important to make it easy for developers to be able to fix issues and extend the functionality.

## Creatable from Archetypes

The components of the architecture should be creatable from architypes so that it can both be easily bootstrapped and also so that it can be updated easily.

## Transparent

Application running in the cloud can be rather opaque with errors being hidden.
The appliation needs to be able to support centralizing logs.

## Platform Agnostic

As much as possible it should be possible to develop  components for the solution on either Windows or Linux.

## Validation

Validation is an important component of many web applications.
For any functionality where data is uploaded it is going to be necessary to validate the data and be as helpful to the user as possible about issues with their data.

The architecture should have services specificaly dedicated validating input and all to maintaining validation error content.

[Further](./Validation.Capability.md)

### Value
`Providing informative validation capability simplifies down stream processing and error handling`

## Secure

The intention here is to facilitate

## In Cloud vs On Premise

The architecture should support and on premise deployment option.

## Direct Cost - Usage Profile

The cost profile for this architecture should approach 0 as the usage of implementation drops to 0.  We are seeking to avoid having any overhead costs.

This desired leads towards use of functions as deployment units rather than servers.

## Multi Tenanted

The architecture intends to support multi-tenancy

[Further](./MultiTenancy.Capability.md)

### Value
`Supports customer scale and provisioning speed`

## Low Integration Authentication

The architecture is intended for B2B low volume use cases where users will come from other organizations.
The intention is to support authenticating external users with minimal B2B coupling as possible.

[Further](./Authentication.Capability.md)

### Value
`To support customer provisioning speed`