# Multi Tenancy

##  Advantages

- Better support for economy of scale
- Better economy of scale for cloud usage
- Better able monitor cloud resource utilization and malicious use
- Better covers off now to pay for support.  Mutlti-tenancy is pretty clearly a subscription model.

## Disadvantages

- Customer may not be happy their IP is on a shared platform

## Mechanisms to enable 

- customer specific DNS maping
- customer specific style configuration
- customer specific storage configuration

## What problems need to be solved

- Storage of tenant specific content onto customer specific tenancy
- Access for users when identity is customer specific directory



