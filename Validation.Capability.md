# Validation

Parsing -> Cleansing -> Validating -> Error Handling

## Parsing

Data sets need to be parsed and what is important is to retain information about the location of errors in the original source.
Parsing can also produce errors

### How To

We can use a framework like [unified](https://unifiedjs.com/) to process input data into an [AST](https://astexplorer.net/#/gist/0a92bbf654aca4fdfb3f139254cf0bad/ffe102014c188434c027e43661dbe6ec30042ee2).

### Rules

- the file is not even parsable

## Cleansing

The capability to take some input and remove records which are invalid.


### How To

Cleanse Function which return the record if valid or and error if invalid

### Rules

- Values should be numbers
- Values are not provided

## Validating

The capability to reject a data set if it fails some aggregate criteria.

### How To

Validate function which is given the data set and returns a set of validation errors.

### Rules

- there are not enough records in the data set
- there are entries with different units

## Error Handling

The ability to generate information errors messages.

Error messages should
- have a code
- have a message
- refer to the source data which is the issue by line number and invalid value

### How To

