# Function Deployment to an Azure Cloud

There are options about how to deploy functions.

- Serverless Azure Deployment
- Deployment Directly to Windows App Services
- Deployment to App Service via Docker images
- The GCP Cloud Run Approach

## Serverless Azure Deployment

This technique was certainly easy to acheive.

The *SLS* command line allows one to easily 
- create the project
- then add functions
- then deploy to Azure

The deployment functionality provisioned the azure resource group, storage account, app service plan and app service

[Here](https://www.serverless.com/framework/docs/providers/azure/guide/quick-start/) is an reference article which was helpful.

The connect to Azure cli

```
az login
az account list //to see available subscription
az account set --subscription <subscription id>

az ad sp create-for-rbac //This creates a service principle (client id and secret) appropriate to interact with azure.
```

It was necessary to define a set of environment variales as the SLS deploy cli using the azure cli under its hood.

```
export AZURE_TENANTID=
export AZURE_SUBSCRIPTION_ID=
export AZURE_CLIENT_ID=
export AZURE_CLIENT_SECRET
```

Then this was used to deploy
```
sls deploy
```

### Docker Function App

The process of deployment via docker was a bit more involved that other techniques but there is no reason this could not be elimated by CICD automation.

Here is a [reference article](https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-function-linux-custom-image?tabs=bash%2Cportal&pivots=programming-language-javascript) which was easy to follow.

The azure core function tools provides a way to create a project that includes an appropriate dockerfile.

```
func init LocalFunctionsProject --worker-runtime node --language javascript --docker
```

It was easy to add new functions again using the `func` cli.

```
func new --name HttpExample --template "HTTP trigger"
```

The project is build using docker and pushed to github.

```
docker build --tag <dockerid>/<imagename> .
docker push <dockerid>/<imagename>
```

Finally the app service is created and reference the dockerhub image

```
az functionapp create --name FnStorageApp --storage-account functionstorateaccont --resource-group AzureFunctionsContainers-rg --plan myPremiumPlan --deployment-container-image-name <dockerid>/<image name>:v1.0.0 --functions-version 3
```

It was then possible to invoke the function.

It was possible to run the service offline using the following in the project.
```
func start
```

### GCP Cloud Run Approach

The GCP approach was very streamlined and introduced the least constrained approach.

- [Cloud Run Getting Started](https://cloud.google.com/run/docs/quickstarts/build-and-deploy) was easy to follow.

- A standard express application is created.
- A standard dockerfile is created.

The following commands builds the project (in the cloud) and deploys the container to google GCR.
```
gcloud builds submit --tag gcr.io/ontrack-265102/hello-world
```

A second command deploys and starts the container in cloudrun.
```
gcloud run deploy --image gcr.io/ontrack-265102/hello-world --platform managed
```