# A Web Application Architecture 2020

<img src="./images/WebAppReferenceArchitecture.png" width=500/>

## Goals for the Architecture

[see Architecture Goals](./Architecture.Goals.md)

## Capbilities

- [How to authenticate used](./Authentication.Capability.md)
- [How to validate inputs](./Validation.Capability.md)
- [How to acheive multitenancy](./MultiTenancy.Capability.md)
- [How to deploy functions/services rather than servers](./FunctionDeployment.Capability.md)

## References

- The [Department of Defense](https://dodcio.defense.gov/Library/DoD-Architecture-Framework/dodaf20_services/) services viewpoints provide a comprehensive mechanism to consider an architecture and it will be worthwhile to describe this architecture in terms of the viewpoints described in this framework.

## Validations


- [How to secure services](./Security.ServiceAccess.md)
- [How to support dyanmic service addition](./Change.ByAddition.md)
- [How to centralize logs](./Operate.Logging.md)
